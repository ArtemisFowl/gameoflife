import typing
from typing import Callable

sizecol, sizerow = 10, 10

# Implement infinite boards
# Implement steps
class Board:
    def __init__(self, sizecol: int, sizerow: int):
        self.sizecol, self.sizerow = sizecol, sizerow
        self.data = [[0 for tile in range(sizerow)] for row in range(sizecol)]
        self.tile_reprs = {
            'alive': 1,
            'dead': 0
        }

    def is_inside(self, col: int, row: int) -> bool:
        return 0 <= col < self.sizecol and 0 <= row < self.sizerow

def get_neighbours(board: Board, col: int, row: int) -> list[int]:
    offsets = [
        (col + offcol - 1, row + offrow - 1) 
        for offcol in range(3) for offrow in range(3)
        if (offcol != 1 or offrow != 1)
    ]
    neighbours = []
    for offcol, offrow in offsets:
        if board.is_inside(offcol, offrow):
            neighbours.append(board.data[offcol][offrow])
    return neighbours

def output(board):
    for row in board.data:
        row_repr = ""
        for tile in row:
            row_repr += f"{tile} "
        print(row_repr)
    print()

def loop(current_state: str, func: Callable, board: Board):
    print(current_state)
    output(board)
    input_message = "(Y/n/int) > "
    continuation = input(input_message)
    while continuation not in ['No', 'N', 'n']:
        try:
            continuation = int(continuation)
        except:
            continuation = 1
        for i in range(continuation):
            board = func(board)
            output(board)
        continuation = input(input_message)
    return board

# 1 means alive, 0 means dead
# refactor so only 1 return is used
def iteration(board: Board) -> Board:
    for col, current_row in enumerate(board.data):
        for row, tile in enumerate(current_row):
            neighbours = get_neighbours(board, col, row)
            neighbours_alive = neighbours.count(
                    board.tile_reprs['alive']
            )
            should_live = int(
                    (neighbours_alive == 2 and tile) or neighbours_alive == 3
            )
            if tile != should_live:
                board.data[col][row] = should_live
                return board
    return board

def init(board: Board):
    col     = int(input("col > "))
    row     = int(input("row > "))
    state   = input("state> ")
    if board.is_inside(col, row):
        # needs to invert
        if state.lower() not in board.tile_reprs:
            print("Cell type does not exist.")
        else:
            board.data[col][row] = board.tile_reprs[state]
    else:
        print("Not in board")
    return board

def main(board):
    board = loop("Init", init, board)
    board = loop("Iteration", iteration, board)

if __name__ == '__main__':
    sizecol, sizerow = 2, 2
    sizecol = int(input("col size> "))
    sizerow = int(input("row size> "))
    board = Board(sizecol, sizerow)
    main(board)
